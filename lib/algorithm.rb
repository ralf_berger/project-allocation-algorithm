# coding: utf-8
class Algorithm
  def self.run(courses, students)
    incomplete_allocations = [ Allocation.new(students, courses) ]
    next_allocation        =   nil
    best_solution          =   nil

    while true
      i = (i || 0) + 1
      current_allocation = next_allocation || incomplete_allocations.sort_by{ |a| -a.total_score }.first # + a.depth * 0.001
      break if current_allocation.nil?
      # puts "#{current_allocation}, score: #{current_allocation.total_score}"

      incomplete_allocations -= [ current_allocation ]
      # puts "#{incomplete_allocations.count} incomplete allocations left" if i % 1000 == 0

      if current_allocation.is_leaf?
        best_solution   = current_allocation
        puts "#{current_allocation}, score: #{current_allocation.total_score}, min: #{current_allocation.min_score}"
        break if current_allocation.min_score == -1
        incomplete_allocations.reject!{ |a| a.total_score <= best_solution.total_score }
        next_allocation = nil
      else
        new_allocations = current_allocation.children
        new_allocations.reject!{ |a| a.total_score <= best_solution.total_score } if best_solution
        incomplete_allocations += new_allocations
        next_allocation = new_allocations.max_by{ |a| a.total_score }
      end
    end

    best_solution
  end
end
