# coding: utf-8
class Allocation
  attr_accessor :students     # array of student's preferences
  attr_accessor :courses      # array of free spaces left per course
  attr_reader   :parent       # the parent allocation
  attr_reader   :course       # the course used for this allocation
  attr_reader   :total_score  # nummeric score for current total (un-)happiness ^^
  attr_reader   :min_score    # the worst individual score contained
  attr_reader   :allocations  # array mapping students (index) to course
  attr_accessor :children

  def initialize(students, courses, parent = nil, course = nil)
    @students    = students.clone
    @courses     = courses.clone

    if parent
      @parent      = parent
      @total_score = parent.total_score
      @min_score   = parent.min_score
      @allocations = parent.allocations.clone
    else
      @total_score = 0
      @min_score   = 0
      @allocations = []
    end

    if course
      student = @students.shift
      throw "No students left." if student == nil

      @course = course
      @allocations << course
      if @courses[course] > 0
        @courses[course] -= 1
      else
        throw "Course #{course} has no space left."
      end
      if student.include? course
        @score = -(student.index course) # First wish: -0, second: -1, etc.
      else
        @score = -(student.count + 5) # Three wishes, none fulfilled: - x
      end

      @total_score = @total_score + @score
      @min_score   = [@min_score, @score].min
    end
  end

  def is_leaf?
    @is_leaf ||= ! (courses_with_free_spaces.count > 0 && @students.count > 0)
  end

  def depth
    @depth ||= @allocations.count
  end

  def courses_with_free_spaces
    @courses_with_free_spaces ||= courses_with_index.reject{ |index, spaces| spaces < 1 }.sort_by{ |index, spaces| spaces }.map{ |index, spaces| index }
  end

  def courses_with_index
    @courses.map.with_index{ |spaces, index| [index, spaces] }
  end

  def children
    return [] if is_leaf?
    @children ||= courses_with_free_spaces.map do |course_index|
      Allocation.new @students, @courses, self, course_index
    end
  end

  def to_s
    @to_s ||= parent ? (parent.to_s + '-' + to_colored_s) : '|'
  end

  def to_colored_s
    color = case @score
    when 0
      :green
    when -1
      :yellow
    when -2
      :red
    when -3
      :red
    else
      :blue
    end
    @course.to_s.send color
  end
end
