# coding: utf-8
require 'bundler'
Bundler.require

class String
  include Term::ANSIColor
end

require './lib/allocation'
require './lib/algorithm'
