#!/usr/bin/env ruby
# coding: utf-8
require './configuration'

# project $INDEX has $N free places
courses  = [12, 12, 12, 12, 12, 12, 12, 12, 12]

# student $INDEX picked the courses $ARRAY
students = (1..108).map{ (0..(courses.count - 1)).to_a.sample(3) }

puts "\nAssigning #{students.count} students to #{courses.count} courses with #{courses.inject(&:+)} places in total:".bold

solution = Algorithm.run courses, students

puts "\nOptimal solution:".bold
puts solution ? solution : 'None found'
